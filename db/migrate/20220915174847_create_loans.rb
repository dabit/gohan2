class CreateLoans < ActiveRecord::Migration[7.0]
  def change
    create_table :loans do |t|
      t.integer :amount
      t.integer :rate
      t.uuid :account_uid

      t.timestamps
    end
  end
end
